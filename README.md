# Halcyon

< Screenshot coming to you soon >

## Features:

Note: not all are implemented yet. This theme is not release ready.

* Black, elegant theme
* Single column
* Quicksand and Roboto font (easy to change!)
* Works fine on desktop and mobile
* Built in Font Awesome support!
* Disqus support
* Google Analytics support
* Asks nicely if the user would like to use these external services

## Technical:
* Well structured, easy to modify
* Built with Stylus
* Templates in Pug
* And a hint of Coffeescript(?)
* Uses CSS3 and HTML5. Praised be Flexbox!
* GPL licensed
* Serves fonts locally, supporting noscript users well! No JS necessary.
