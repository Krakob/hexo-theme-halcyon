These themes are from highlight.js

https://github.com/isagalaev/highlight.js

Due to an issue in Hexo, the stylesheets are modified to use highlight.js' old styling. This should be reverted once the Hexo issue is resolved.

https://github.com/hexojs/hexo-util/issues/19
